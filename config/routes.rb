Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :books
  resources :authors
  # get '/authors', :to => 'authors#indez'
  root 'authors#index'
end
