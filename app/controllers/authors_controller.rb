class AuthorsController < ApplicationController

	before_action :set_author, only: [:update, :edit,:delete, :show]

	def new
		@author = Author.new
	end

	def create
		@author = Author.new(author_params)
		respond_to do |format|
      if @author.save
        format.html { redirect_to @author, notice: 'successfully created.' }
        format.json { render :show, status: :created, location: @author }
      else
        format.html { render :new }
        format.json { render json: @author.errors, status: :unprocessable_entity }
      end
    end
	end

	def index
		@authors = Author.all
	end

	def update
		respond_to do |format|
      if @author.update(author_params)
        format.html { redirect_to @author, notice: 'successfully updated.' }
        format.json { render :show, status: :ok, location: @author }
      else
        format.html { render :edit }
        format.json { render json: @author.errors, status: :unprocessable_entity }
      end
    end
	end

	def show
		@author
	end

	def edit

	end

	def delete

	end

	private

		def set_author
			@author = Author.find params["id"]
		end
		
		def author_params
			params.require(:author).permit(:id, :name, book_ids: [])
		end

end
